package com.rest.service;

import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.rest.model.Employee;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	

    @Mock
    private RestTemplate restTemplate;
 
    @InjectMocks
    private UserService userService = new UserService();
	
    @Test
	public void getAllEmployees() {
    	
    	

		List<Employee> list = new ArrayList<>();
		Employee emp = new Employee(1, "Jansi", "Tirupur", "9566497854", new Date(2020-10-10));
		Employee emp1 = new Employee(2, "Jenni", "Coimbatore", "9566497855", new Date(2020-02-10));
		list.add(emp);
		list.add(emp1);
		Mockito
        .when(restTemplate.getForEntity(
          "http://laptop-lip6dhcr:8084/EmployeeRest/employees", Employee.class))
        .thenReturn(Mockito.any());
		String employee = userService.getAllEmployees();
		
		Assert.assertEquals(employee, list.getClass());
	}

}
