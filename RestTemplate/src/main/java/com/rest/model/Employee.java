package com.rest.model;

import java.util.Date;


public class Employee {

	private int empId;

	private String name;

	private String address;

	private String mobileNo;

	private Date DOJ;

	private int deptId;


	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}



	public Employee() {
		super();
	}
	
	
	
	

	public Employee(int empId, String name, String address, String mobileNo, Date dOJ) {
		super();
		this.empId = empId;
		this.name = name;
		this.address = address;
		this.mobileNo = mobileNo;
		DOJ = dOJ;
		
	}


	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Date getDOJ() {
		return DOJ;
	}

	public void setDOJ(Date dOJ) {
		DOJ = dOJ;
	}

	
}
