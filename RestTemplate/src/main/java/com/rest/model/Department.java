package com.rest.model;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class Department {

	private int deptId;

	private String deptName;

	@Temporal(TemporalType.DATE)
	private Date createdAt;

	public Department() {
		super();
	}

	public Department(int deptId, String deptName, Date createdAt) {
		super();
		this.deptId = deptId;
		this.deptName = deptName;
		this.createdAt = createdAt;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return "Department [deptId=" + deptId + ", deptName=" + deptName + ", createdAt=" + createdAt + "]";
	}

}
