package com.rest.service;

import java.util.Date;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.rest.model.Department;
import com.rest.model.Employee;

@Service
public class UserService {

	@Autowired
	RestTemplate restTemplate;

	@Bean
	RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	public String getAllEmployees() {

		String result = restTemplate.getForObject("http://laptop-lip6dhcr:8084/EmployeeRest/employees", String.class);

		return result;
	}

	public String getAllDepartments() {
		String result = restTemplate.getForObject("http://laptop-lip6dhcr:8085/DepartmentRest/departments",
				String.class);
		return result;
	}

	public String getEmployeeById(String empId) {
		String url = "http://laptop-lip6dhcr:8084/EmployeeRest/employees/employee1/" + empId;
		String result = restTemplate.getForObject(url, String.class);
		return result;
	}

	public String getDepartmentById(String deptId) {
		String url = "http://laptop-lip6dhcr:8085/DepartmentRest/departments/department1/" + deptId;
		String result = restTemplate.getForObject(url, String.class);
		return result;
	}

	public ResponseEntity<String> createEmployee(@RequestBody Employee employee) {
		String uri = "http://laptop-lip6dhcr:8084/EmployeeRest/employees/employee";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();

		request.put("doj", new Date(2020 - 1900, 10 - 1, 12));
		request.put("address", "Pune");
		request.put("mobileNo", "9065787658");
		request.put("name", "Rajkumar");

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		return response;
	}

	public ResponseEntity<String> createDepartment(Department department) {
		String uri = "http://laptop-lip6dhcr:8085/DepartmentRest/departments/department";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		JSONObject request = new JSONObject();

		request.put("createdAt", new Date(2019 - 1900, 12 - 1, 13));
		request.put("deptName", "Arch");

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> response = restTemplate.postForEntity(uri, entity, String.class);

		System.out.println(response);
		return response;
	}

	public Department updateDepartment(Department department) {
		String uri = "http://laptop-lip6dhcr:8085/DepartmentRest/departments/department6";

		Department dept = new Department(2, "CSE Department", new Date(2019 - 1900, 10, 10));

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(uri, dept, department.getClass());
		return dept;

	}

	public Employee updateEmployee(Employee employee) {
		String uri = "http://laptop-lip6dhcr:8084/EmployeeRest/employees/employee6";

		Employee emp = new Employee(4, "Diwahar", "Delhi", "9876897657", new Date(2019 - 1900, 07, 05));

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.put(uri, emp, employee.getClass());
		return emp;

	}

	public void deleteEmployee(String empId) {
		String uri = "http://laptop-lip6dhcr:8084/EmployeeRest/employees/employee/" + empId;

		restTemplate.delete(uri, String.class);

	}

	public void deleteDepartment(String deptId) {

		String uri = "http://laptop-lip6dhcr:8085/DepartmentRest/departments/department/" + deptId;

		restTemplate.delete(uri, String.class);

	}

}
