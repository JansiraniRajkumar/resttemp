package com.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.model.Department;
import com.rest.model.Employee;
import com.rest.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping
	public String getEmployees() {
		return userService.getAllEmployees();
	}

	@GetMapping(value = "/dept")
	public String getDepartments() {
		return userService.getAllDepartments();
	}

	@GetMapping("/{userId}")
	public String getEmployeeById(@PathVariable String userId) {
		return userService.getEmployeeById(userId);
	}

	@GetMapping("/department/{deptId}")
	public String getDepartmentById(@PathVariable String deptId) {
		return userService.getDepartmentById(deptId);
	}

	@PostMapping("/employee")
	public ResponseEntity<String> createEmployee(Employee employee) {
		return userService.createEmployee(employee);
	}

	@PostMapping("/department1")
	public ResponseEntity<String> createDepartment(Department department) {
		return userService.createDepartment(department);
	}
	
	@PutMapping("/department3")
	public Department updateDepartment(Department department) {
		return userService.updateDepartment(department);
	}
	
	@PutMapping("/employee3")
	public Employee updateEmployee(Employee employee) {
		return userService.updateEmployee(employee);
	}
	
	@DeleteMapping("/employee7/{id}")
	public void deleteEmployee(@PathVariable("id") String id) {
		 userService.deleteEmployee(id);
	}
	
	
	
	@DeleteMapping("/department7/{id}")
	public void deleteDepartment(@PathVariable("id") String id) {
		 userService.deleteDepartment(id);
	}

}
