package com.rest.service;

import java.util.List;

import com.rest.model.Department;



public interface DepartmentService {
	

	public List<Department> getallDepartments();

	public Department updateDepartment(Department department);

	public Department addDepartment(Department department);

	public void deleteDepartment(int deptId);

	public Department getDepartmentById(int deptId);

}
