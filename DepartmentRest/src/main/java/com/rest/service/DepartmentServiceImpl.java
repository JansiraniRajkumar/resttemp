package com.rest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rest.dao.DepartmentRepository;
import com.rest.exception.DepartmentNotFoundException;
import com.rest.model.Department;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository deptRepository;

	@Override
	public List<Department> getallDepartments() {
		List<Department> department = new ArrayList<>();
		deptRepository.findAll().forEach(department::add);
		return department;
	}

	@Override
	public Department updateDepartment(Department department) {
		Optional<Department> dept = deptRepository.findById(department.getDeptId());

		if (dept.isPresent()) {
			Department newEntity = dept.get();
			newEntity.setDeptId(department.getDeptId());
			newEntity.setDeptName(department.getDeptName());
			newEntity.setCreatedAt(department.getCreatedAt());
			newEntity = deptRepository.save(newEntity);
			return newEntity;
		} else {
			department = deptRepository.save(department);
			return department;

		}
	}

	@Override
	public Department addDepartment(Department department) {
		return deptRepository.save(department);
	}

	@Override
	public void deleteDepartment(int deptId) {
		deptRepository.deleteById(deptId);

	}

	@Override
	public Department getDepartmentById(int deptId) {
		Optional<Department> dept = deptRepository.findById(deptId);
		Department department = null;
		if (dept.isPresent()) {
			department = dept.get();
		} else {

			throw new DepartmentNotFoundException("Department with given id " + deptId + " Not Found ");
		}
		return department;
	}

}
