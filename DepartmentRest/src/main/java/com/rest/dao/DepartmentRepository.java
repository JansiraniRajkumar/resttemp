package com.rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.model.Department;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer>{

}
