package com.rest.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Department")
public class Department {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DeptId")
	private int deptId;
	@Column(name = "DeptName")
	private String deptName;
	@Column(name = "CreatedAt")
	@Temporal(TemporalType.DATE)
	private Date createdAt;

	public Department() {
		super();
	}

	public Department(int deptId, String deptName, Date createdAt) {
		super();
		this.deptId = deptId;
		this.deptName = deptName;
		this.createdAt = createdAt;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public String toString() {
		return "Department [deptId=" + deptId + ", deptName=" + deptName + ", createdAt=" + createdAt + "]";
	}

}
