package com.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.model.Department;
import com.rest.service.DepartmentService;


@RestController
@RequestMapping("/departments")
public class DepartmentController {
	

	@Autowired
	private DepartmentService deptService;

	@PostMapping(value = "/department")
	public ResponseEntity<Department> addDepartment(@Valid @RequestBody Department department) {
		Department dept = deptService.addDepartment(department);
		return new ResponseEntity<Department>(dept, HttpStatus.OK);

	}

	@GetMapping
	public ResponseEntity<List<Department>> getAllDepartments() {
		List<Department> department = deptService.getallDepartments();
		return new ResponseEntity<List<Department>>(department, HttpStatus.OK);
	}

	@GetMapping(value = "/department1/{id}")
	public ResponseEntity<Department> getDepartment(@PathVariable("id") int id) {
		Department department = deptService.getDepartmentById(id);
		return new ResponseEntity<Department>(department, HttpStatus.OK);

	}

	@PutMapping(value = "/department6")
	public ResponseEntity<Department> updateDepartment(@Valid @RequestBody Department department) {
		Department updated = deptService.updateDepartment(department);
		return new ResponseEntity<Department>(updated, HttpStatus.OK);
	}

	@DeleteMapping(value = "/department/{id}")
	public ResponseEntity<String> deleteDepartment(@PathVariable("id") int deptId) {
		deptService.deleteDepartment(deptId);
		return new ResponseEntity<String>("Department deleted", HttpStatus.OK);
	}



}
