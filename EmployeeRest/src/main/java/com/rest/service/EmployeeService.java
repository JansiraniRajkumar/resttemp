package com.rest.service;

import java.util.List;

import com.rest.model.Employee;

public interface EmployeeService {
	

	public List<Employee> getallEmployees();

	public Employee updateEmployee(Employee employee);

	public Employee addEmployee(Employee employee);

	public void deleteEmployee(int empId);

	public Employee getEmployeeById(int empId);

}
