package com.rest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rest.dao.EmployeeRepository;
import com.rest.exception.EmployeeNotFoundException;
import com.rest.model.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository empRepository;

	@Override
	public List<Employee> getallEmployees() {
		List<Employee> emp = new ArrayList<>();
		empRepository.findAll().forEach(emp::add);
		return emp;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		Optional<Employee> emp = empRepository.findById(employee.getEmpId());

		if (emp.isPresent()) {
			Employee newEntity = emp.get();
			newEntity.setEmpId(employee.getEmpId());
			newEntity.setName(employee.getName());
			newEntity.setAddress(employee.getAddress());
			newEntity.setMobileNo(employee.getMobileNo());
			newEntity.setDOJ(employee.getDOJ());
			newEntity.setDepartment(employee.getDepartment());
			newEntity = empRepository.save(newEntity);
			return newEntity;
		} else {
			employee = empRepository.save(employee);
			return employee;

		}
	}

	@Override
	public Employee addEmployee(Employee employee) {
		return empRepository.save(employee);
	}

	@Override
	public void deleteEmployee(int empId) {
		empRepository.deleteById(empId);
		
	}

	@Override
	public Employee getEmployeeById(int empId) {
		Optional<Employee> emp = empRepository.findById(empId);
		Employee employee= null;
		if(emp.isPresent()) {
				employee = emp.get();
		}else {
			
		throw new EmployeeNotFoundException("Employee with given id " + empId + " Not Found ");
		}
		return employee;
	}

}
