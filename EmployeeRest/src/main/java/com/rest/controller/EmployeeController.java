package com.rest.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.model.Employee;
import com.rest.service.EmployeeService;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

	@Autowired
	private EmployeeService empService;

	@PostMapping(value = "/employee")
	public ResponseEntity<Employee> addEmployee(@Valid @RequestBody Employee employee) {
		Employee emp = empService.addEmployee(employee);
		return new ResponseEntity<Employee>(emp, HttpStatus.OK);

	}

	@GetMapping
	public ResponseEntity<List<Employee>> getAllEmployees() {
		List<Employee> employee = empService.getallEmployees();
		return new ResponseEntity<List<Employee>>(employee, HttpStatus.OK);
	}

	@GetMapping(value = "/employee1/{id}")
	public ResponseEntity<Employee> getEmployee(@PathVariable("id") int id) {
		Employee employee = empService.getEmployeeById(id);
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);

	}

	@PutMapping(value = "/employee6")
	public ResponseEntity<Employee> updateEmployee(@Valid @RequestBody Employee employee) {
		Employee updated = empService.updateEmployee(employee);
		return new ResponseEntity<Employee>(updated, HttpStatus.OK);
	}

	@DeleteMapping(value = "/employee/{id}")
	public ResponseEntity<String> deleteEmployee(@PathVariable("id") int empId) {
		empService.deleteEmployee(empId);
		return new ResponseEntity<String>("Employee deleted", HttpStatus.OK);
	}

}
